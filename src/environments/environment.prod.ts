export const environment = {
  production: true
};

export const API = {
  currentWeatherApi: 'https://api.openweathermap.org/data/2.5/weather',
  forecastApi: 'https://api.openweathermap.org/data/2.5/forecast',
  appId: '1c6548c886b69f3c7409e3beb7664ddc'
}
