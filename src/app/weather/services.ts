/**
 * Main service for the API calls. The urls are injected from the environment files. 
 **/
import { Injectable } from '@angular/core';
import { API } from '../../environments/environment';
import { HttpClient, HttpResponse, HttpHeaders, HttpEventType } from '@angular/common/http';

const currentWeatherUrl = API.currentWeatherApi;
const forecastUrl = API.forecastApi;
const appId = API.appId;
@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  constructor(
    private _http: HttpClient
  ) { }

  getCurentWeather(data:Object) {
    return this._http.get(currentWeatherUrl,{
        //   set the query params here. Or else to remove redundancy the params can be defined above too.
        params: {
            q: data['name'] + ',' +data['country'],
            APPID: appId
        }
    });
  }

  getForecast(data:Object) {
    return this._http.get(forecastUrl,{
        params: {
            q: data['name'] + ',' +data['country'],
            APPID: appId
        }
    });
  }

}