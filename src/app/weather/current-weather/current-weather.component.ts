import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { WeatherService } from '../services';

@Component({
  selector: 'app-current-weather',
  templateUrl: './current-weather.component.html',
  styleUrls: ['./current-weather.component.css']
})
export class CurrentWeatherComponent implements OnInit {
  @Input() data: Object;
  @Output() forecast:EventEmitter<any> = new EventEmitter();
  clickForecast: boolean;
  apiData: Object;
  avgTemp: number;
  constructor(private weatherService:WeatherService) { }

  displayForecast() {
    // Trigger the eventemitter for subscribing by the parent component.
    this.forecast.emit(this.data);

    // Hiding the button after click.
    this.clickForecast = true;
  }

  ngOnInit() {
    this.clickForecast = false;

    // Call the API through the service.
    this.weatherService.getCurentWeather(this.data).subscribe(data => {
      this.apiData = data;
      this.avgTemp = (data['main']['temp_max'] + data['main']['temp_min'])/2;
    },
    err => {
      console.log(err);
    })
  }

}
