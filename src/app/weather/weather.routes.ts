import { Routes } from '@angular/router';

import { WeatherComponent } from './weather/weather.component';


export const HomeRoutes: Routes = [
  { path: '', component: WeatherComponent }
];