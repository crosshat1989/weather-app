import { 
  Component,
  ViewChild,
  ViewContainerRef,
  ComponentFactoryResolver,
  Type,
  OnInit } from '@angular/core';
import { CurrentWeatherComponent } from '../current-weather/current-weather.component';
import { ForecastComponent } from '../forecast/forecast.component';
// interface for the city data
export interface city {
  name: string;
  country: string;
}
@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.css']
})
export class WeatherComponent implements OnInit {
  componentRef: any;
  cities: city[] = [
    {name: 'Amsterdam', country: 'Netherlands'},
    {name: 'Berlin', country: 'Germany'},
    {name: 'Paris', country: 'France'},
    {name: 'London', country: 'UK'},
    {name: 'Frankfurt', country: 'Germany'}
  ];

  @ViewChild('parent', { read: ViewContainerRef }) entry: ViewContainerRef;

  constructor(private resolver: ComponentFactoryResolver) { }

  getCurrentWeather(val:Object) {
    this.entry.clear(); //Clear the component tree before creating a new one.

    // Registering the component to the Factory.
    const factory = this.resolver.resolveComponentFactory(CurrentWeatherComponent);

    // Instantiating the component.
    this.componentRef = this.entry.createComponent(factory);

    //Passing data to the component instance.
    this.componentRef.instance.data = val; 

    //Subscribing to the eventemitter from the component instance and rendering the forcast component.
    this.componentRef.instance.forecast.subscribe((data: Object) => {

      // Repeat the component instantiating steps again here with the forecast component.
      const factory = this.resolver.resolveComponentFactory(ForecastComponent);
      this.componentRef = this.entry.createComponent(factory);
      this.componentRef.instance.data = data
    })
  }

  ngOnInit() {
    // Setting default value to the first object of the array.
    this.getCurrentWeather(this.cities[0])
  }

}
