import { Component, OnInit, Input } from '@angular/core';
import { WeatherService } from '../services';
@Component({
  selector: 'app-forecast',
  templateUrl: './forecast.component.html',
  styleUrls: ['./forecast.component.css']
})
export class ForecastComponent implements OnInit {
@Input() data: Object;
apiData: Object;
mapData: Array<Object>;
  constructor(private weatherService: WeatherService) { }

  ngOnInit() {
    // Clear the array on every call.
    this.mapData = [];

    // Call the API through the service.
    this.weatherService.getForecast(this.data).subscribe(data => {
      // The mapping can be further made robust by creating an interface based on the JSON from the API.
      // The code below calculates the average temperature from the max and min temperature and creates an object which is pushed into the array.
      data['list'].map((element:number) => {
        this.mapData.push(
          {
            avgTemp: (element['main']['temp_max'] + element['main']['temp_min'])/2,
            windSpeed: element['wind']['speed'],
            time: element['dt_txt'] 
          });
      });
    },
    err => {
      console.log(err);
    });
  }

}
