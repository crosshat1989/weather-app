import { Routes } from '@angular/router';

export const rootRouterConfig: Routes = [
    {
      path: '',
      redirectTo: 'home',
      pathMatch: 'full'
    },
    {
      path: 'home',
      loadChildren: './weather/weather.module#WeatherModule'
    }
]